<?php
/**
 * A basic port of John Walker's MoonTool http://www.fourmilab.ch/
 * @author George Steel <george@net-glue.co.uk>
 * @copyright Copyright (c) 2012 Net Glue Ltd (http://netglue.co)
 * @license http://opensource.org/licenses/MIT
 * @package	Netglue_MoonTool
 * @link https://bitbucket.org/netglue/moontool
 */

namespace Netglue\Moon;

/**
 * A basic port of John Walker's MoonTool http://www.fourmilab.ch/
 * @author George Steel <george@net-glue.co.uk>
 * @copyright Copyright (c) 2012 Net Glue Ltd (http://netglue.co)
 * @license http://opensource.org/licenses/MIT
 * @package	Netglue_MoonTool
 * @link https://bitbucket.org/netglue/moontool
 */
class MoonTool {
	
	/**
	 * Astronomical Constants
	 */
	
	/**
	 * 1980 January 0.0
	 */
	const EPOCH = 2444238.5;
	
	/**
	 * Constants defining the sun's apparent orbit
	 */
	
	/**
	 * Ecliptic longitude of the Sun at epoch 1980.0
	 */
	const E_LONG_E = 278.833540;
	
	/**
	 * Ecliptic longitude of the Sun at perigee
	 */
	const E_LONG_P = 282.596403;
	
	/**
	 * Eccentricity of Earth's orbit
	 */
	const ECCENT = 0.016718;
	
	/**
	 * Semi-major axis of Earth's orbit, km
	 */
	const SUNSMAX = 1.495985e8;
	
	/**
	 * Sun's angular size, degrees, at semi-major axis distance
	 */
	const SUNANGSIZ = 0.533128;
	
	/**
	 * Elements of the Moon's orbit, epoch 1980.0
	 */
	
	/**
	 * Moon's mean lonigitude at the epoch
	 */
	const MMLONG = 64.975464;
	
	/**
	 * Mean longitude of the perigee at the epoch
	 */
	const MMLONGP = 349.383063; 
	
	/**
	 * Mean longitude of the node at the epoch
	 */
	const MLNODE = 151.950429;
	
	/**
	 * Inclination of the Moon's orbit
	 */
	const MINC = 5.145396;
	
	/**
	 * Eccentricity of the Moon's orbit
	 */
	const MECC = 0.054900;
	
	/**
	 * Moon's angular size at distance a from Earth
	 */
	const MANGSIZ = 0.5181;
	
	/**
	 * Semi-major axis of Moon's orbit in km
	 */
	const MSMAX = 384401.0;
	
	/**
	 * Parallax at distance a from Earth
	 */
	const MPARALLAX = 0.9507;
	
	/**
	 * Synodic month (new Moon to new Moon)
	 */
	const SYNMONTH = 29.53058868;
	
	/**
	 * Base date for E. W. Brown's numbered series of lunations (1923 January 16)
	 */
	const LUNATBASE = 2423436.0;
	
	/**
	 * Properties of the Earth
	 */
	const EARTHRAD = 6378.16; /* Radius of Earth in kilometres */
	
	/**
	 * Phase name integer constants
	 */
	const PHASE_NEW = 0;
	const PHASE_Q1 = 1;
	const PHASE_FULL = 2;
	const PHASE_Q3 = 3;
	
	/**
	 * Unix timestamp used as a context for computing phase times
	 * @var int|NULL
	 */
	protected $_time;
	
	/**
	 * The julian Day number corresponding to the timestamp in $_time
	 * @var int
	 */
	protected $_julianDay;
	
	/**
	 * The julian time corresponding to the timestamp in $_time
	 * @var float
	 */
	protected $_julianTime;
	
	/**
	 * Fraction fill of the moon at $_time
	 * @see MoonTool::phase()
	 * @var float|NULL
	 */
	protected $_moonPhase;
	
	/**
	 * The illuminated fraction of the moon at $_time
	 * A float between 0 and 1
	 * Calculated in phase()
	 * @see MoonTool::phase()
	 * @var float|NULL
	 */
	protected $_illuminatedFraction;
	
	/**
	 * The age of the moon in days
	 * @var float|NULL
	 */
	protected $_moonAge;
	
	/**
	 * The distance of the moon from the center of the earth in kilometers
	 * @var float|NULL
	 */
	protected $_distance;
	
	/**
	 * Angular diameter of the moon, whatever that is??
	 * @var float|NULL
	 */
	protected $_angularDiameter;
	
	/**
	 * Distance to the sun in KM
	 * @var float|NULL
	 */
	protected $_distanceToSun;
	
	/**
	 * Angular diameter of the sun
	 * @var float|NULL
	 */
	protected $_sunAngularDiameter;
	
	/**
	 * Static property for storing computed phase times
	 * @var array
	 */
	public static $phaseCache = array();
	
	/**
	 * Construct with a unix timestamp
	 * @param int $time Current time is used if null
	 * @return void
	 */
	public function __construct($time = NULL) {
		if(null === $time) {
			$time = time();
		}
		$this->setTime($time);
	}
	
	/**
	 * Return the time of the new moon after the given time
	 * @param int $time
	 * @return int Unix timestamp
	 */
	public function getNextNewMoon($time) {
		$jTime = self::getJulianTime($time);
		$adate = $jTime;
		$yy = $mm = $dd = NULL;
		self::jYear($adate, $yy, $mm, $dd);
		$k1 = floor(($yy + (($mm - 1) * (1.0 / 12.0)) - 1900) * 12.3685);
		$adate = $nt1 = self::meanPhase($adate, $k1);
		while (TRUE) {
			$adate += self::SYNMONTH;
			$k2 = $k1 + 1;
			$nt2 = self::meanPhase($adate, $k2);
			if($nt1 <= $jTime && $nt2 > $jTime) {
				break;
			}
			$nt1 = $nt2;
			$k1 = $k2;
		}
		return self::julianToTimestamp(self::truePhase($k2, 0.0));
	}
	
	/**
	 * Return an array containing all phases in the given year
	 * @param int $year
	 * @return array
	 */
	public static function getPhasesInYear($year) {
		self::_fillCacheArray($year);
		$time = mktime(0,0,0,1,1,$year);
		$julianTime = self::getJulianTime($time);
		$yearEnd = self::getJulianTime(mktime(0,0,0,1,1,($year+1)));
		
		$yy = $mm = $dd = NULL;
		self::jYear($julianTime, $yy, $mm, $dd);
		
		$k1 = floor(($yy + (($mm - 1) * (1.0 / 12.0)) - 1900) * 12.3685);
		while(true) {
			$dayOfNewMoon = self::meanPhase($julianTime, $k1);
			self::_cachePhases(self::getPhasesFromIndex($k1));
			if($dayOfNewMoon > $yearEnd) {
				break;
			}
			$k1 +=1;
		}
		return self::$phaseCache[$year];
	}
	
	/**
	 * Return an array containing the times of the phases in the given month
	 * @param int $year
	 * @param int $month
	 * @return array
	 */
	public static function getPhasesInMonth($year, $month) {
		self::getPhasesInYear($year);
		return self::$phaseCache[$year][$month];
	}
	
	/**
	 * Return phase times for a cycle stating at the new moon synodix index time
	 * @param float $index A julian time
	 * @return array An array indexed by phase type (Starting with New Moon at index 0) containing unix time stamps for each phase
	 */
	protected static function getPhasesFromIndex($index) {
		$out = array();
		for($i = 0; $i <= 3; $i++) {
			$k = $i * 0.25;
			$j = self::truePhase($index, $k);
			$t = self::julianToTimestamp($j);
			$out[$i] = $t;
		}
		return $out;
	}
	
	/**
	 * Helper method to store computed phase times in statuc property
	 * @param array $phases
	 * @return void
	 */
	protected function _cachePhases(array $phases) {
		foreach($phases as $type => $time) {
			self::_cachePhaseTime($time, $type);
		}
	}
	
	/**
	 * Helper method to store a computed phase time in a static property
	 * @param int $time Unix timestamp
	 * @param int $phaseType one of the phase type constants
	 * @return void
	 */
	protected static function _cachePhaseTime($time, $phaseType) {
		$y = (int) date("Y", $time);
		$m = (int) date("n", $time);
		$d = (int) date("j", $time);
		self::$phaseCache[$y][$m]['times'][$time] = $phaseType;
		self::$phaseCache[$y][$m]['dates'][$d] = $phaseType;
	}
	
	/**
	 * Helper method to initialise array structures for cached computations for the given year
	 * @param int $year
	 * @return void
	 */
	protected static function _fillCacheArray($year) {
		for($y = $year - 1; $y <= $year + 1; $y++) {
			if(!isset(self::$phaseCache[$y])) {
				self::$phaseCache[$y] = array();
			}
			for($m = 1; $m <= 12; $m++) {
				if(!isset(self::$phaseCache[$y][$m])) {
					self::$phaseCache[$y][$m] = array(
						'dates' => array(),
						'times' => array(),
					);
				}
			}
		}
	}
	
	/**
	 * Set the time at which to perform calculations
	 * @param int $time
	 */
	public function setTime($time) {
		$this->_time = $time;
		$this->_julianDay = $this->getJulianDayNumber($this->_time);
		$this->_julianTime = $this->getJulianTime($this->_time);
		$this->phase($this->_julianTime);
	}
	
	/**
	 * Return the time used as a basis for calculation
	 * @return int Unix timestamp
	 */
	public function getTime() {
		if(null === $this->_time) {
			$this->setTime(time());
		}
		return $this->_time;
	}
	
	/**
	 * Return the age of the moon as an array containing days, hours and minutes
	 * @return array
	 */
	public function getMoonAgeArray() {
		if(null === $this->_time) {
			$this->setTime(time());
			$this->phase($this->_julianTime);
		}
		$aom_d = (int) $this->_moonAge;
		$aom_h = (int) (24 * ($this->_moonAge - floor($this->_moonAge)));
		$aom_m = (int) (1440 * ($this->_moonAge - floor($this->_moonAge))) % 60;
		return array(
			'days' => $aom_d,
			'hours' => $aom_h,
			'minutes' => $aom_m,
		);
	}
	
	/**
	 * Return the illuminated fraction of the moon for the current set time
	 * The returned value is a float between 0 and 1. This method will return the
	 * the illuminated fraction of the moon using the current time if no time has been set
	 *
	 * @return float
	 */
	public function getIlluminatedFraction() {
		if(null === $this->_time) {
			$this->setTime(time());
			$this->phase($this->_julianTime);
		}
		if(is_null($this->_illuminatedFraction)) {
			$this->phase($this->_julianTime);
		}
		return $this->_illuminatedFraction;
	}
	
	/**
	 * Return the times of the phases found around the specified timestamp
	 * This method searches backwards from $time to find a new moon, then finds all
	 * phases forward of that new moon up to the next new moon. So index 0 and 4
	 * of the returned array are both times of new moons, meaning index 2 is full
	 * and indexes 1 and 3 are first and last quarter respectively
	 * Essentially, this is a helper for MoonTool::phaseHunt() that uses unix
	 * timestamps instead of julian dates
	 * @param int $time Unix timestamp
	 * @return array
	 */
	public static function doPhaseHunt($time) {
		$p = self::phaseHunt(self::getJulianTime($time));
		foreach($p as $k => $jt) {
			$p[$k] = self::julianToTimestamp($jt);
		}
		return $p;
	}
	
	/**
	 * Helper method for dumping some information
	 * @return string
	 */
	public function dump() {
		$nptime = 0.0;
		$aom_d = (int) $this->_moonAge;
		$aom_h = (int) (24 * ($this->_moonAge - floor($this->_moonAge)));
		$aom_m = (int) (1440 * ($this->_moonAge - floor($this->_moonAge))) % 60;
		$out = "\n";
		$out .= " Universal Time: ".date("j/n/Y H:i:s", $this->_time)."\n";
		$out .= "     Julian Day: ".round($this->_julianTime, 5)."\n";
		$out .= "    Age of moon: {$aom_d} days, {$aom_h} hours, {$aom_m} minutes\n";
		$out .= "     Moon Phase: ".intval($this->_illuminatedFraction * 100)."%\n";
		$out .= "Moon's Distance: ".round($this->_distance, 2).' kilometers, '.round($this->_distance / self::EARTHRAD, 4).' Earth radii'."\n";
		$out .= "  Moon subtends: ".round($this->_angularDiameter, 5)." degrees\n";
		$out .= " Sun's distance: ".round($this->_distanceToSun).' kilometers, '.($this->_distanceToSun / self::SUNSMAX).' astronomical units'."\n";
		$out .= "   Sun subtends: ".round($this->_sunAngularDiameter, 4)." degrees\n";
		$phases = $this->phaseHunt($this->_julianTime + 0.5);
		$names = array(
			'  Last New Moon: ',
			'  First Quarter: ',
			'      Full Moon: ',
			'   Last Quarter: ',
			'  Next New Moon: ',
		);
		foreach($phases as $k => $phase) {
			$time = $this->julianToTimestamp($phase);
			$out .= $names[$k].date("j/n/Y H:i", $time)."\n";
		}
		
		$nptime = $phases[4];
		return $out;
	}
	
	/**
	 * Julian time to Unix timestamp
	 * @param float $jtime
	 * @return int
	 */
	public function julianToTimestamp($jtime) {
		$y = $m = $d = $h = $i = $s = NULL;
		self::jYear($jtime, $y, $m, $d);
		self::jhms($jtime, $h, $i, $s);
		return mktime($h, $i, $s, $m, $d, $y);
	}
	
	/**
	 * Return the times of the phases found around the specified julian time
	 * This method searches backwards from $julianTime to find a new moon, then finds all
	 * phases forward of that new moon up to the next new moon. So index 0 and 4
	 * of the returned array are both times of new moons, meaning index 2 is full
	 * and indexes 1 and 3 are first and last quarter respectively
	 * @param float $julianTime
	 * @return array
	 */
	public static function phaseHunt($julianTime) {
		$adate = $julianTime - 45;
		$yy = $mm = $dd = NULL;
		self::jYear($adate, $yy, $mm, $dd);
		$k1 = floor(($yy + (($mm - 1) * (1.0 / 12.0)) - 1900) * 12.3685);
		$adate = $nt1 = self::meanPhase($adate, $k1);
		while (TRUE) {
			$adate += self::SYNMONTH;
			$k2 = $k1 + 1;
			$nt2 = self::meanPhase($adate, $k2);
			if($nt1 <= $julianTime && $nt2 > $julianTime) {
				break;
			}
			$nt1 = $nt2;
			$k1 = $k2;
		}
		$phases[0] = self::truePhase($k1, 0.0);
		$phases[1] = self::truePhase($k1, 0.25);
		$phases[2] = self::truePhase($k1, 0.5);
		$phases[3] = self::truePhase($k1, 0.75);
		$phases[4] = self::truePhase($k2, 0.0);
		return $phases;
	}
	
	/**
	 * Return the next and previous new moon julian times surrounding the specified unix time stamp
	 * @param int $time Unix Timestamp
	 * @param float &$previous
	 * @param float &$next
	 * @return void
	 */
	public static function getSynodicMonthIndex($time, &$previous, &$next) {
		$julianTime = self::getJulianTime($time);
		$adate = $julianTime - 45;
		$yy = $mm = $dd = NULL;
		self::jYear($adate, $yy, $mm, $dd);
		$k1 = floor(($yy + (($mm - 1) * (1.0 / 12.0)) - 1900) * 12.3685);
		$adate = $nt1 = self::meanPhase($adate, $k1);
		while (TRUE) {
			$adate += self::SYNMONTH;
			$k2 = $k1 + 1;
			$nt2 = self::meanPhase($adate, $k2);
			if($nt1 <= $julianTime && $nt2 > $julianTime) {
				break;
			}
			$nt1 = $nt2;
			$k1 = $k2;
		}
		$previous = $k1;
		$next = $k2;
	}
	
	/**
	 * Get the year month and day of the given julian time fraction or date
	 * @param float $jd
	 * @param int &$yy
	 * @param int &$mm
	 * @param int &$dd
	 * @return void
	 */
	public static function jYear($jd, &$yy, &$mm, &$dd) {
		$jd += 0.5; // Astronomical to civil
		$j = floor($jd);
		$j = $j - 1721119.0;
		$y = floor(((4 * $j) - 1) / 146097.0);
		$j = ($j * 4.0) - (1.0 + (146097.0 * $y));
		$d = floor($j / 4.0);
		$j = floor(((4.0 * $d) + 3.0) / 1461.0);
		$d = ((4.0 * $d) + 3.0) - (1461.0 * $j);
		$d = floor(($d + 4.0) / 4.0);
		$m = floor(((5.0 * $d) - 3) / 153.0);
		$d = (5.0 * $d) - (3.0 + (153.0 * $m));
		$d = floor(($d + 5.0) / 5.0);
		$y = (100.0 * $y) + $j;
		if ($m < 10.0) {
			$m = $m + 3;
		} else {
			$m = $m - 9;
			$y = $y + 1;
		}
		$yy = $y;
		$mm = $m;
		$dd = $d;
	}
	
	/**
	 * Get the Hours, minutes and seconds of the day given a julian time fraction
	 * @param float $jd
	 * @param int &$h
	 * @param int &$m
	 * @param int &$s
	 * @return void
	 */
	public static function jhms($jd, &$h, &$m, &$s) {
		$jd += 0.5;				/* Astronomical to civil */
		$ij = ($jd - floor($jd)) * 86400.0;
		$h = $ij / 3600;
		$m = ($ij / 60) % 60;
		$s = $ij % 60;
	}
	
	/**
	 * Calculates time of the mean new Moon for a given base date.
	 * This argument K to this function is the precomputed synodic month index,
	 * given by:
	 *   K = (year - 1900) * 12.3685
	 * where year is expressed as a year and fractional year.
	 * @param float $sdate
	 * @param float $k
	 * @return float
	 */
	public static function meanPhase($sdate, $k) {
		/* Time in Julian centuries from 1900 January 0.5 */
		$t = ($sdate - 2415020.0) / 36525;
		$t2 = $t * $t;
		$t3 = $t2 * $t;
		
		$nt1 = 2415020.75933 + self::SYNMONTH * $k
		+ 0.0001178 * $t2
		- 0.000000155 * $t3
		+ 0.00033 * self::_dsin(166.56 + 132.87 * $t - 0.009173 * $t2);
		return $nt1;
	}
	
	/**
	 * truePhase()
	 * Given a K value used to determine the mean phase of the new moon, and a phase
	 * selector (0.0, 0.25, 0.5, 0.75), obtain the true, corrected phase time.
	 * @param float $k
	 * @param float $phase
	 * @return float
	 */
	public static function truePhase($k, $phase) {
		//double t, t2, t3, pt, m, mprime, f;
		//int apcor = FALSE;
		$apcor = false;
		$k += $phase; /* Add phase to new moon time */
		$t = $k / 1236.85; /* Time in Julian centuries from 1900 January 0.5 */
		$t2 = $t * $t; /* Square for frequent use */
		$t3 = $t2 * $t; /* Cube for frequent use */
		/* Mean time of phase */
		$pt = 2415020.75933
			+ self::SYNMONTH * $k
			+ 0.0001178 * $t2
			- 0.000000155 * $t3
			+ 0.00033 * self::_dsin(166.56 + 132.87 * $t - 0.009173 * $t2);
		/* Sun's mean anomaly */
		$m = 359.2242
			+ 29.10535608 * $k
			- 0.0000333 * $t2
			- 0.00000347 * $t3;
		/* Moon's mean anomaly */
		$mprime = 306.0253
			+ 385.81691806 * $k
			+ 0.0107306 * $t2
			+ 0.00001236 * $t3;
		/* Moon's argument of latitude */
		$f = 21.2964
			+ 390.67050646 * $k
			- 0.0016528 * $t2
			- 0.00000239 * $t3;
		
		if (($phase < 0.01) || (abs($phase - 0.5) < 0.01)) {
			/* Corrections for New and Full Moon */
			$pt += (0.1734 - 0.000393 * $t) * self::_dsin($m)
				+ 0.0021 * self::_dsin(2 * $m)
				- 0.4068 * self::_dsin($mprime)
				+ 0.0161 * self::_dsin(2 * $mprime)
				- 0.0004 * self::_dsin(3 * $mprime)
				+ 0.0104 * self::_dsin(2 * $f)
				- 0.0051 * self::_dsin($m + $mprime)
				- 0.0074 * self::_dsin($m - $mprime)
				+ 0.0004 * self::_dsin(2 * $f + $m)
				- 0.0004 * self::_dsin(2 * $f - $m)
				- 0.0006 * self::_dsin(2 * $f + $mprime)
				+ 0.0010 * self::_dsin(2 * $f - $mprime)
				+ 0.0005 * self::_dsin($m + 2 * $mprime);
			$apcor = TRUE;
		} elseif ((abs($phase - 0.25) < 0.01 || (abs($phase - 0.75) < 0.01))) {
			$pt += (0.1721 - 0.0004 * $t) * self::_dsin($m)
				+ 0.0021 * self::_dsin(2 * $m)
				- 0.6280 * self::_dsin($mprime)
				+ 0.0089 * self::_dsin(2 * $mprime)
				- 0.0004 * self::_dsin(3 * $mprime)
				+ 0.0079 * self::_dsin(2 * $f)
				- 0.0119 * self::_dsin($m + $mprime)
				- 0.0047 * self::_dsin($m - $mprime)
				+ 0.0003 * self::_dsin(2 * $f + $m)
				- 0.0004 * self::_dsin(2 * $f - $m)
				- 0.0006 * self::_dsin(2 * $f + $mprime)
				+ 0.0021 * self::_dsin(2 * $f - $mprime)
				+ 0.0003 * self::_dsin($m + 2 * $mprime)
				+ 0.0004 * self::_dsin($m - 2 * $mprime)
				- 0.0003 * self::_dsin(2 * $m + $mprime);
			if($phase < 0.5) {
				/* First quarter correction */
	      $pt += 0.0028 - 0.0004 * self::_dcos($m) + 0.0003 * self::_dcos($mprime);
	    } else {
				/* Last quarter correction */
				$pt += -0.0028 + 0.0004 * self::_dcos($m) - 0.0003 * self::_dcos($mprime);
			}
			$apcor = TRUE;
		}
		if(!$apcor) {
			throw new Exception("truePhase() called with invalid phase selector");
		}
		return $pt;
	}
	
	/**
	 * KEPLER  --	Solve the equation of Kepler.
	 * @param float $m
	 * @param float $ecc
	 * @return float
	 */
	public static function kepler($m, $ecc) {
		$EPSILON = 1E-6;
		$e = $m = deg2rad($m);
		do {
			$delta = $e - $ecc * sin($e) - $m;
			$e -= $delta / (1 - $ecc * cos($e));
		} while (abs($delta) > $EPSILON);
		return $e;
	}
	
	/**
	 * PHASE  --  Calculate phase of moon as a fraction:
	 *
	 * The argument is the time for which the phase is requested,
	 * expressed as a Julian date and fraction.  Returns the terminator
	 * phase angle as a percentage of a full circle (i.e., 0 to 1),
	 * and stores into pointer arguments the illuminated fraction of
	 * the Moon's disc, the Moon's age in days and fraction, the
	 * distance of the Moon from the centre of the Earth, and the
	 * angular diameter subtended by the Moon as seen by an observer
	 * at the centre of the Earth.
	 * @param float $julianTime
	 * @return float Fill percentage from 0 - 1
	 */
	public function phase ($julianTime) {
//		double	pdate;
// 		double	*pphase;		/* Illuminated fraction */
// 		double	*mage;			/* Age of moon in days */
// 		double	*dist;			/* Distance in kilometres */
// 		double	*angdia;		/* Angular diameter in degrees */
// 		double	*sudist;		/* Distance to Sun */
// 		double  *suangdia;	/* Sun's angular diameter */
//		double	Day, N, M, Ec, Lambdasun, ml, MM, MN, Ev, Ae, A3, MmP,
//			mEc, A4, lP, V, lPP, NP, y, x, Lambdamoon, BetaM,
//			MoonAge, MoonPhase,
//			MoonDist, MoonDFrac, MoonAng, MoonPar,
//			F, SunDist, SunAng;
	
		/* Calculation of the Sun's position */

		$Day = $julianTime - self::EPOCH; /* Date within epoch */
		$N = self::_fixAngle(((360 / 365.2422) * $Day));	/* Mean anomaly of the Sun */
		$M = self::_fixangle($N + self::E_LONG_E - self::E_LONG_P); /* Convert from perigee co-ordinates to epoch 1980.0 */
		$Ec = self::kepler($M, self::ECCENT); /* Solve equation of Kepler */
		$Ec = sqrt((1 + self::ECCENT) / (1 - self::ECCENT)) * tan($Ec / 2);
		$Ec = 2 * rad2deg(atan($Ec)); /* True anomaly */
		$Lambdasun = self::_fixAngle($Ec + self::E_LONG_P); /* Sun's geocentric ecliptic longitude */
		/* Orbital distance factor */
		$F = ((1 + self::ECCENT * cos(deg2rad($Ec))) / (1 - self::ECCENT * self::ECCENT));
		$SunDist = self::SUNSMAX / $F; /* Distance to Sun in km */
		$SunAng = $F * self::SUNANGSIZ; /* Sun's angular size in degrees */
		
		/* Calculation of the Moon's position */
		/* Moon's mean longitude */
		$ml = self::_fixAngle((13.1763966 * $Day) + self::MMLONG);
		
		/* Moon's mean anomaly */
		$MM = self::_fixAngle($ml - 0.1114041 * $Day - self::MMLONGP);
		
		/* Moon's ascending node mean longitude */
		$MN = self::_fixAngle(self::MLNODE - 0.0529539 * $Day);
	
		/* Evection */
		$Ev = 1.2739 * sin(deg2rad(2 * ($ml - $Lambdasun) - $MM));
		
		/* Annual equation */
		$Ae = 0.1858 * sin(deg2rad($M));
		
		/* Correction term */
		$A3 = 0.37 * sin(deg2rad($M));
	
		/* Corrected anomaly */
		$MmP = $MM + $Ev - $Ae - $A3;
		
		/* Correction for the equation of the centre */
		$mEc = 6.2886 * sin(deg2rad($MmP));
		
		/* Another correction term */
		$A4 = 0.214 * sin(deg2rad(2 * $MmP));
		
		/* Corrected longitude */
		$lP = $ml + $Ev + $mEc - $Ae + $A4;
		
		/* Variation */
		$V = 0.6583 * sin(deg2rad(2 * ($lP - $Lambdasun)));
		
		/* True longitude */
		$lPP = $lP + $V;
		
		/* Corrected longitude of the node */
		$NP = $MN - 0.16 * sin(deg2rad($M));
		
		/* Y inclination coordinate */
		$y = sin(deg2rad($lPP - $NP)) * cos(deg2rad(self::MINC));
		
		/* X inclination coordinate */
		$x = cos(deg2rad($lPP - $NP));
		
		/* Ecliptic longitude */
		$Lambdamoon = rad2deg(atan2($y, $x));
		$Lambdamoon += $NP;
		
		/* Ecliptic latitude */
		$BetaM = rad2deg(asin(sin(deg2rad($lPP - $NP)) * sin(deg2rad(self::MINC))));
		
		/* Calculation of the phase of the Moon */
		
		/* Age of the Moon in degrees */
		$MoonAge = $lPP - $Lambdasun;
		
		/* Phase of the Moon */
		$MoonPhase = (1 - cos(deg2rad($MoonAge))) / 2;
		
		/* Calculate distance of moon from the centre of the Earth */
		
		$MoonDist = (self::MSMAX * (1 - self::MECC * self::MECC)) / (1 + self::MECC * cos(deg2rad($MmP + $mEc)));
		
    /* Calculate Moon's angular diameter */
		
		$MoonDFrac = $MoonDist / self::MSMAX;
		$MoonAng = self::MANGSIZ / $MoonDFrac;
		
    /* Calculate Moon's parallax */
		
		$MoonPar = (self::MPARALLAX / $MoonDFrac);

		
		$this->_illuminatedFraction = $MoonPhase;
		$this->_moonAge = self::SYNMONTH * (self::_fixAngle($MoonAge) / 360.0);
		$this->_distance = $MoonDist;
		$this->_angularDiameter = $MoonAng;
		$this->_distanceToSun = $SunDist;
		$this->_sunAngularDiameter = $SunAng;
		$this->_moonPhase = self::_fixAngle($MoonAge) / 360.0;
		return $this->_moonPhase;
	}
	
	/**
	 * Utility Math Methods found in original
	 * PHP has builtin:
	 *  abs()
	 *  deg2rad()
	 *  rad2deg()
	 */
	public static function _fixAngle($a) {
		return (($a) - 360.0 * (floor(($a) / 360.0)));
	}
	
	public static function _dsin($x) {
		return sin(deg2rad($x));
	}
	
	public static function _dcos($x) {
		return cos(deg2rad($x));
	}
	
	/**
	 * Return the Julian Day number for the specified year, month and day
	 * @return int
	 */
	public static function getJulianDayNumber($time) {
		$year = (int) date("Y", $time);
		$month = (int) date("n", $time);
		$day = (int) date("j", $time);
		$a = intval((14 - $month) / 12);
		$y = $year + 4800 - $a;
		$m = $month + (12 * $a) - 3;
		$jdn = $day
			+ intval((( (153 * $m) + 2 ) / 5))
			+ ( 365 * $y )
			+ intval($y / 4)
			- intval($y / 100)
			+ intval($y / 400)
			- 32045;
		return (int) $jdn;
	}
	
	/**
	 * Return the julian day as a floating point number
	 * @param int $time Unix timestamp
	 * @return float
	 */
	public static function getJulianTime($time) {
		$jd = self::getJulianDayNumber($time);
		$h = (int) date("H", $time);
		$m = (int) date("i", $time);
		$s = (int) date("s", $time);
		$jd = $jd - 0.5;
		$jt = ($s + (60  * ( $m + 60 * $h ))) / 86400.0;
		return ($jd + $jt);
	}
	
	
}