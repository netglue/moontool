# INSTALLATION

Put the moon tool class file somewhere on your include path and include it manually, i.e.

	require_once 'path/to/MoonTool.php';

You could install it with composer.
	
	php composer.phar install netglue/moontool

