<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<title>Moon Tool Examples</title>
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/2.2.1/bootstrap.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/highlight.js/7.3/highlight.min.js"></script>
	
	<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/2.2.1/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/highlight.js/7.3/styles/solarized_dark.min.css">
	
	<script>
  hljs.tabReplace = '    ';
  hljs.initHighlightingOnLoad();
  </script>
</head>
<body>
	
	<div class="navbar navbar-inverse navbar-fixed-top">
		<div class="navbar-inner">
			<div class="container">
				<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				</a>
				<a class="brand" href="#">PHP Moontool</a>
				<div class="nav-collapse collapse">
			<ul class="nav">
			<li><a href="https://bitbucket.org/netglue/moontool">About</a></li>
			<li><a href="https://bitbucket.org/netglue/moontool/issues">Issue Tracker</a></li>
			</ul>
			
			</div><!--/.nav-collapse -->
			</div>
		</div>
	</div>
	
	<div class="container">
		
		<div class="hero-unit">
			<h1>PHP Port of Moon Tool</h1>
			<p>Examples are provided below for usage of the <code>Netglue\Moon\MoonTool</code> class</p>
		</div>
		
		<?php
		require_once '../library/Netglue/Moon/MoonTool.php';
		use \Netglue\Moon\MoonTool as Moon;
		$mt = new Moon;
		$phaseNames = array(
			Moon::PHASE_NEW => 'New Moon',
			Moon::PHASE_Q1 => 'First Quarter',
			Moon::PHASE_FULL => 'Full Moon',
			Moon::PHASE_Q3 => 'Third Quarter',
		);
		?>
		<div class="row">
			<div class="span4">
				<h2>Next New Moon</h2>
				<?php
				$next = $mt->getNextNewMoon(time());
				?>
				<p>The next new moon is on <em><?php echo date('l jS F \a\t H:i', $next); ?></em></p>
				<pre><code class="php"><?php echo htmlentities('require_once \'../library/Netglue/Moon/MoonTool.php\';
use \Netglue\Moon\MoonTool as Moon;
$mt = new Moon;
$next = $mt->getNextNewMoon(time());'); ?></code></pre>
			</div>
			
			<div class="span4">
				<h2>Moon Information</h2>
				<?php
				$time = mktime(12, 0, 0, 1, 1, 2000);
				$mt->setTime($time);
				$illuminated = $mt->getIlluminatedFraction();
				$age = $mt->getMoonAgeArray();
				?>
				<p>On <?php echo date("l jS F Y", $time); ?> at midday, the moon was <?php echo sprintf('%0.2f', $illuminated * 100); ?>% illuminated and was
				<?php echo sprintf('%d days, %d hours and %d minutes old', $age['days'], $age['hours'], $age['minutes']); ?></p>
				<pre><code class="php"><?php echo htmlentities('$time = mktime(12, 0, 0, 1, 1, 2000);
$mt->setTime($time);
$illuminated = $mt->getIlluminatedFraction();
$age = $mt->getMoonAgeArray();
echo sprintf(\'The moon is %d days, %d hours and %d minutes old\', $age[\'days\'], $age[\'hours\'], $age[\'minutes\']);'); ?></code></pre>
			</div>
			
			<div class="span4">
				<h2>Phases this month</h2>
				<dl>
				<?php
				$phases = $mt->getPhasesInMonth(date("Y"), date("n"));
				foreach($phases['times'] as $time => $phase) {
					?>
					<dt><?php echo date('l jS F \a\t H:i', $time); ?></dt>
					<dd><?php echo $phaseNames[$phase]; ?></dd>
					<?php
				}
				?>
				</dl>
				<pre><code class="php"><?php echo htmlentities('$phases = $mt->getPhasesInMonth(date("Y"), date("n"));'); ?></code></pre>
			</div>
		</div>
		
		<?php
		$yearPhases = $mt->getPhasesInYear(date("Y"));
		?>
		
		<div class="well well-small">
			<table class="table">
				<caption>Moon Phases in <?php echo date("Y"); ?></caption>
				<thead>
					<tr>
						<th></th>
						<th>New Moon</th>
						<th>First Quarter</th>
						<th>Full Moon</th>
						<th>Third Quarter</th>
					</tr>
				</thead>
				<tbody>
				<?php
				foreach($yearPhases as $month => $data) {
					$new = $full = $q1 = $q3 = array();
					$date = date("F Y", key($data['times']));
					$rows = 0;
					$markup = array();
					while(current($data['times']) !== false) {
						$row = array();
						for($i = 0; $i <= 3; $i++) {
							if(current($data['times']) == $i) {
								$row[] = '<td>'.date('l jS \a\t H:i', key($data['times'])).'</td>';
								next($data['times']);
							} else {
								$row[] = '<td></td>';
							}
						}
						$markup[] = implode("\n", $row);
						$rows++;
					}
					?>
					<tr>
						<th rowspan="<?php echo $rows; ?>"><?php echo $date; ?></th>
						<?php echo current($markup); ?>
					</tr>
					<?php
					$next = next($markup);
					if($next !== false) {
						echo "\n<tr>".$next.'</tr>';
					}
				}
				?>
				</tbody>
			</table>
		</div>
		
	</div>
	
	
	
	
</body>
</html>
