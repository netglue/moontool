#Net Glue Moon Tool Port

This is port in PHP of the moontool C program written by John Walker
[http://www.fourmilab.ch/](http://www.fourmilab.ch/)

I know nothing about planetary physics, but managed to muddle my way through
the code and mostly figure out what was going on. There's a lot of static methods
going on... Anyway, the times it produces are consistent with what
[NASA has to say](http://eclipse.gsfc.nasa.gov/phase/phasecat.html) so
I'm happy!

I originally put it together to help decorate tide tables with the lunar phases
for a client and thought it might be useful to others so here it is.

There is little inline documentation of the source - and it could do with a clean up
and some extra love. I'll get around to it at some point...

If you use this utility, I'd love to know about it and hope it turns out to be useful.
You can get in touch at [netglue.co/contact](http://netglue.co/contact)

## GETTING MOONTOOL

Get a copy of the source from BitBucket at
	
	git clone git@bitbucket.org:netglue/moontool.git

Install it into your project using composer.
	
	require "netglue/moontool": "dev-master"


### SYSTEM REQUIREMENTS

PHP 5.3+

### INSTALLATION

Please see INSTALL.md.
